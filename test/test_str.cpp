// GMock
#include <gmock/gmock.h>
#include <gtest/gtest.h>

// Own
extern "C" {
#include "str.h"
} // extern "C"

struct DoubleCase
{
    DoubleCase(const std::string& s, double d)
        : sval{s}
        , dval{d}
    {
    }

    std::string sval;
    double dval;
};

class DoubleTestWithParam : public testing::TestWithParam<DoubleCase> { };

TEST(String, CanBeDefaultConstructed)
{
    auto* s{str_new()};
    ASSERT_THAT(s, testing::Ne(nullptr));
    str_free(s);
}

TEST(String, IsEmptyWhenDefaultConstructed)
{
    auto* s{str_new()};
    ASSERT_THAT(str_empty(s), testing::Eq(true));
    str_free(s);
}

TEST(String, HasLengthZeroWhenDefaultConstructed)
{
    auto* s{str_new()};
    ASSERT_THAT(str_length(s), testing::Eq(0));
    str_free(s);
}

TEST(String, CanBeConstCharConstructed)
{
    const char* data{"foo bar baz"};
    auto* s{str_new_const_char(data)};
    ASSERT_THAT(s, testing::Ne(nullptr));
    str_free(s);
}

TEST(String, ContainsDataWhenConstCharConstructed)
{
    const char* data{"foo bar baz"};
    auto* s{str_new_const_char(data)};
    ASSERT_THAT(str_get(s), testing::StrEq(data));
    str_free(s);
}

TEST(String, MatchesLengthWhenConstCharConstructed)
{
    const char* data{"foo bar baz"};
    auto* s{str_new_const_char(data)};
    ASSERT_THAT(str_length(s), testing::Eq(strlen(data)));
    str_free(s);
}

TEST(String, CanBeSubstringConstructed)
{
    auto* s1{str_new_const_char("Hello World!")};
    auto* s2{str_new_substr(s1, 6, 6)};
    auto  expected{"World!"};
    ASSERT_THAT(str_get(s2), testing::StrEq(expected));
    str_free(s1);
    str_free(s2);
}

TEST(String, IsEmptyWhenNegativePositionIsGivenToSubstringConstruction)
{
    auto* s1{str_new_const_char("Hello World!")};
    auto* s2{str_new_substr(s1, -1, 6)};
    ASSERT_THAT(str_empty(s2), testing::Eq(true));
    str_free(s1);
    str_free(s2);
}

TEST(String, IsEmptyWhenNULLIsGivenForSubstringConstruction)
{
    auto s{str_new_substr(NULL, 10, 20)};
    ASSERT_THAT(str_empty(s), testing::Eq(true));
    str_free(s);
}

TEST(String, IsEmptyWhenEmptySourceStringIsGivenForSubstringConstruction)
{
    auto s1{str_new()};
    auto s2{str_new_substr(s1, 10, 20)};
    ASSERT_THAT(str_empty(s2), testing::Eq(true));
    str_free(s1);
    str_free(s2);
}

TEST(String, CopiesTheRestOfSubstringWhenCountIsNegative)
{
    auto* s1{str_new_const_char("Hello World!")};
    auto* s2{str_new_substr(s1, 6, -1)};
    auto  expected{"World!"};
    ASSERT_THAT(str_get(s2), testing::StrEq(expected));
    str_free(s1);
    str_free(s2);
}

TEST(String, CopiesTheRestOfSubstringWhenCountIsGreaterThanTheRemainder)
{
    auto* s1{str_new_const_char("Hello World!")};
    auto* s2{str_new_substr(s1, 6, 7)};
    auto  expected{"World!"};
    ASSERT_THAT(str_get(s2), testing::StrEq(expected));
    str_free(s1);
    str_free(s2);
}

TEST(String, CanBeAsciiConstructed)
{
    auto* s{str_new_chr(static_cast<int>('A'))};
    ASSERT_THAT(str_get(s), testing::StrEq("A"));
    str_free(s);
}

TEST(String, IsEmptyWhenAsciiPositionIsNegative)
{
    auto* s{str_new_chr(-1)};
    ASSERT_THAT(str_empty(s), testing::Eq(true));
    str_free(s);
}

TEST(String, IsEmptyWhenAsciiPositionIsGreaterThan255)
{
    auto* s{str_new_chr(256)};
    ASSERT_THAT(str_empty(s), testing::Eq(true));
    str_free(s);
}

TEST(String, ReturnsACharacterAtGivenPosition)
{
    auto* s{str_new_const_char("Hello World!")};
    ASSERT_THAT(str_get_n(s, 6), testing::Eq('W'));
    str_free(s);
}

TEST(String, ReturnZeroWhenPositionIsNegativeForCharacterRetrieval)
{
    auto* s{str_new_const_char("Hello World!")};
    ASSERT_THAT(str_get_n(s, -1), testing::Eq(0));
    str_free(s);
}

TEST(String, ReturnZeroWhenPositionIsGreaterThanStringLengthForCharacterRetrieval)
{
    auto* s{str_new_const_char("Hello World!")};
    ASSERT_THAT(str_get_n(s, 32), testing::Eq(0));
    str_free(s);
}

TEST(String, CanAddCharactersToString)
{
    std::string expected{"Hello World!"};
    auto* s{str_new()};
    for (auto c : expected)
        str_append(s, c);
    ASSERT_THAT(str_get(s), testing::StrEq(expected));
    str_free(s);
}

TEST(String, RefusesToAppendStringWhenMaximumCapacityReached)
{
    std::string data    {"I had a little pony, his name was Dapple-grey"};
    std::string expected{"I had a little pony, his name w"};
    auto* s{str_new()};
    for (auto c : data)
        str_append(s, c);
    ASSERT_THAT(str_get(s), testing::StrEq(expected));
    str_free(s);
}

TEST(String, RefusesToAppendStringWhenNULLIsPassed)
{
    ASSERT_THAT(str_append(NULL, 'a'), testing::Eq(false));
}

TEST(String, ContainsEmptyStringWhenCleared)
{
    auto* s{str_new_const_char("foo bar baz")};
    str_clear(s);
    ASSERT_THAT(str_get(s), testing::Eq(nullptr));
    str_free(s);
}

TEST(String, HasLengthZeroWhenCleared)
{
    auto* s{str_new_const_char("foo bar baz")};
    str_clear(s);
    ASSERT_THAT(str_length(s), testing::Eq(0));
    str_free(s);
}

TEST(String, CanCopyContentToDifferentString)
{
    auto* s1{str_new_const_char("foo bar baz")};
    auto* s2{str_new()};
    str_cpy(s2, s1);
    ASSERT_THAT(str_get(s2), testing::StrEq(str_get(s1)));
    str_free(s1);
    str_free(s2);
}

TEST(String, MatchesLengthWhenCopiedFromDifferentString)
{
    auto* s1{str_new_const_char("foo bar baz")};
    auto* s2{str_new()};
    str_cpy(s2, s1);
    ASSERT_THAT(str_length(s2), testing::Eq(str_length(s1)));
    str_free(s1);
    str_free(s2);
}

TEST(String, CanCopyContentFromConstCharToDifferentString)
{
    const auto* s1{"foo bar baz"};
    auto* s2{str_new()};
    str_cpy_const(s2, s1);
    ASSERT_THAT(str_get(s2), testing::StrEq(s1));
    str_free(s2);
}

TEST(String, MatchesLengthWhenCopiedFromDifferentConstChar)
{
    const auto* s1{"foo bar baz"};
    auto* s2{str_new()};
    str_cpy_const(s2, s1);
    ASSERT_THAT(str_length(s2), testing::Eq(strlen(s1)));
    str_free(s2);
}

TEST(String, CanConcatenate2Strings)
{
    auto* s1{str_new_const_char("Hello ")};
    auto* s2{str_new_const_char("World!")};
    auto* s3{str_new()};
    str_cat(s3, s1, s2);
    ASSERT_THAT(str_get(s3), testing::StrEq("Hello World!"));
    str_free(s1);
    str_free(s2);
    str_free(s3);
}

TEST(String, MatchesLengthWhen2StringsAreConcatenated)
{
    auto* s1{str_new_const_char("Hello ")};
    auto* s2{str_new_const_char("World!")};
    auto* s3{str_new()};
    str_cat(s3, s1, s2);
    ASSERT_THAT(str_length(s3), testing::Eq(strlen("Hello World!")));
    str_free(s1);
    str_free(s2);
    str_free(s3);
}

TEST(String, RefusesToConcatenateWhen2StringsExceedMaximumCapacity)
{
    auto* s1{str_new_const_char("Some very very long string")};
    auto* s2{str_new_const_char("Some other loong string, which wont fit")};
    auto* s3{str_new()};
    ASSERT_THAT(str_cat(s3, s1, s2), testing::Eq(false));
    str_free(s1);
    str_free(s2);
    str_free(s3);
}

TEST(String, HelloStringIsLowerThanWorldString)
{
    auto* s1{str_new_const_char("Hello")};
    auto* s2{str_new_const_char("World")};
    ASSERT_THAT(str_cmp(s1, s2), testing::Lt(0));
    str_free(s1);
    str_free(s2);
}

TEST(String, WorldStringIsGreaterThanHelloString)
{
    auto* s1{str_new_const_char("World")};
    auto* s2{str_new_const_char("Hello")};
    ASSERT_THAT(str_cmp(s1, s2), testing::Gt(0));
    str_free(s1);
    str_free(s2);
}

TEST(String, EqualStringsAreEqual)
{
    auto* s1{str_new_const_char("Hello")};
    auto* s2{str_new_const_char("Hello")};
    ASSERT_THAT(str_cmp(s1, s2), testing::Eq(0));
    str_free(s1);
    str_free(s2);
}

TEST(String, CanReturnOrdinalValueOfGivenCharacter)
{
    auto* s{str_new_const_char("Hello World!")};
    ASSERT_THAT(str_asc(s, 6), testing::Eq(static_cast<int>('W')));
    str_free(s);
}

TEST(String, ReturnsZeroForOrdinalValueWhenPositionIsNegative)
{
    auto* s{str_new_const_char("Hello World!")};
    ASSERT_THAT(str_asc(s, -1), testing::Eq(0));
    str_free(s);
}

TEST(String, ReturnsZeroForOrdinalValueWhenPositionIsGreaterThanStringLength)
{
    auto* s{str_new_const_char("Hello World!")};
    ASSERT_THAT(str_asc(s, 32), testing::Eq(0));
    str_free(s);
}

TEST(String, CanConvertToInt)
{
    auto expected{12345};
    auto expected_s{std::to_string(expected)};
    auto* s{str_new_const_char(expected_s.c_str())};
    int i;
    str_to_int(s, &i);
    ASSERT_THAT(i, testing::Eq(expected));
    str_free(s);
}

TEST(String, RefusesToConvertIntOutOfRange)
{
    auto expected{12345678901234};
    auto expected_s{std::to_string(expected)};
    auto* s{str_new_const_char(expected_s.c_str())};
    int i;
    ASSERT_THAT(str_to_int(s, &i), testing::Eq(false));
    str_free(s);
}

TEST_P(DoubleTestWithParam, CanConvertStringToDouble)
{
    DoubleCase input{GetParam()};

    auto* s{str_new_const_char(input.sval.c_str())};
    double d;
    str_to_double(s, &d);
    ASSERT_THAT(d, testing::DoubleEq(input.dval));
    str_free(s);
}

DoubleCase doubles[] = {
    { "1.2",        1.2       },
    { "0.123e3",    123.0     },
    { "1234.56e2",  123456.0  },
    { "12345.6e-2", 123.456   },
    { "0.123e+2",   12.3      },
    { "1e-2",       0.01      },
    { "1e3",        1000.0    },
    { "123e+4",     1230000.0 },
};

INSTANTIATE_TEST_CASE_P(IntegerTest, DoubleTestWithParam, testing::ValuesIn(doubles));
