// STL
#include <any>
#include <string>
#include <stdexcept>
#include <fstream>
#include <experimental/filesystem>
#include <memory>

// C
#include <errno.h>
#include <fcntl.h>
#include <stdlib.h>
#include <string.h>

// GMock
#include <gmock/gmock.h>
#include <gtest/gtest.h>

// Own
extern "C" {
#include "err.h"
#include "scanner.h"
} // extern "C"

class TmpFile
{
public:
    explicit TmpFile(
            const std::string& filename,
            const std::string& content = {})
        : tmp_dir_{}
        , tmp_filename_{}
    {
        char dir_template[] = "tmpdir-XXXXXX";
        if (!::mkdtemp(dir_template))
            throw std::invalid_argument{dir_template};

        tmp_dir_ = dir_template;
        tmp_filename_ = tmp_dir_ / filename;

        std::ofstream f{tmp_filename_, std::ios::out};
        f.write(content.c_str(), content.size());
        f.close();
    }

    ~TmpFile()
    {
        std::experimental::filesystem::remove_all(tmp_dir_);
    }

    auto filename() const
    {
        return tmp_filename_.c_str();
    }

private:
    std::experimental::filesystem::path tmp_dir_;
    std::experimental::filesystem::path tmp_filename_;
};

struct TokenCase
{
    TokenCase(const std::string& aLexeme, TokenType aType, std::any&& aValue)
        : lexeme{aLexeme}
        , type{aType}
        , value{std::move(aValue)}
    {
    }

    TokenCase(const std::string& aLexeme, TokenType aType)
        : lexeme{aLexeme}
        , type{aType}
        , value{aLexeme}
    {
    }

    std::string lexeme;
    TokenType type;
    std::any value;
};

bool operator==(TokenCase tc, const Token* t2) try
{
    if (!t2 || tc.type != t2->type)
        return false;

    switch (tc.type) {
    [[fallthrough]]
    case T_IDENTIFIER:
    case T_STRING:
        if (!t2->data.sval)
            return false;
        return str_cmp_const(
                t2->data.sval,
                std::any_cast<std::string>(tc.value).c_str()) == 0;
    case T_INTEGER:
        return std::any_cast<int>(tc.value) == t2->data.ival;
    case T_DOUBLE:
        return std::any_cast<double>(tc.value) == t2->data.dval;
    default:
        return false;
    }
} catch (const std::bad_any_cast& e) {
    return false;
}

class ScanLexeme : public testing::TestWithParam<TokenCase> { };

TEST(Scanner, CanOpenInputFile)
{
    TmpFile f{"foo.vb"};
    ASSERT_THAT(scanner_open(f.filename()), testing::Eq(0));
}

TEST(Scanner, FailsToOpenNonExistingInputFile)
{
    std::string filename{"NonExistingFilename.vb"};
    ASSERT_THAT(scanner_open(filename.c_str()), testing::Eq(ERR_InputFile));
}

TEST(Scanner, CanCloseOpenedInputFile)
{
    TmpFile f{"foo.vb"};
    scanner_open(f.filename());
    ASSERT_THAT(scanner_close(), testing::Eq(ERR_None));
}

TEST(Scanner, FailsToCloseNonOpenedInputFile)
{
    ASSERT_THAT(scanner_close(), testing::Eq(ERR_InputFile));
}

TEST(Scanner, ReturnsNULLTokenForNonOpenedInputFile)
{
    ASSERT_THAT(scanner_get_token(), testing::Eq(nullptr));
}

TEST_P(ScanLexeme, CanParseTokenWithValue)
{
    TokenCase input{GetParam()};

    TmpFile f{"test.vb", input.lexeme};
    scanner_open(f.filename());
    auto t{scanner_get_token()};
    scanner_close();

    ASSERT_THAT(input, testing::Eq(t));
}

TokenCase identifiers[] = {
    { "identifier",                  T_IDENTIFIER },
    { "IdentifierWithUPPERCase",     T_IDENTIFIER },
    { "Identifier_with_underscores", T_IDENTIFIER },
    { "_identifier_",                T_IDENTIFIER },
    { "IdentifierWithNumbers123423", T_IDENTIFIER },
};

INSTANTIATE_TEST_CASE_P(IdentifierTest, ScanLexeme, testing::ValuesIn(identifiers));

TokenCase integers[] = {
    { "65535", T_INTEGER, 65535 },
    { "00001", T_INTEGER,     1 },
    { "80000", T_INTEGER, 80000 },
};

INSTANTIATE_TEST_CASE_P(IntegerTest, ScanLexeme, testing::ValuesIn(integers));

TokenCase doubles[] = {
    { "1.2",        T_DOUBLE, 1.2       },
    { "0.123e3",    T_DOUBLE, 123.0     },
    { "1234.56e2",  T_DOUBLE, 123456.0  },
    { "12345.6e-2", T_DOUBLE, 123.456   },
    { "0.123e+2",   T_DOUBLE, 12.3      },
    { "1e-2",       T_DOUBLE, 0.01      },
    { "1e3",        T_DOUBLE, 1000.0    },
    { "123e+4",     T_DOUBLE, 1230000.0 },
};

INSTANTIATE_TEST_CASE_P(DoubleTest, ScanLexeme, testing::ValuesIn(doubles));

TokenCase strings[] = {
    { "!\"some short string\"", T_STRING,     std::string{"some short string"} },
    { "!\"\"",                  T_STRING,     std::string{}                    },
    { "invalid string",         T_IDENTIFIER, std::string{"invalid"}           },
};

INSTANTIATE_TEST_CASE_P(StringTest, ScanLexeme, testing::ValuesIn(strings));
