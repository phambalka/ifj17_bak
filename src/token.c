// C
#include <string.h>

// Own
#include "token.h"

void token_init(Token* t)
{
    if (!t)
        return;
    switch (t->type) {
    case T_IDENTIFIER:
    case T_STRING:
        str_free(t->data.sval);
        break;
    default:
        break;
    }
    memset(t, 0, sizeof(Token));
}
