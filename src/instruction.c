
#include <stdlib.h>
#include "instruction.h"
#include "error.h"

instruction *inst_create(enum inst_type type, union inst_operand src1, union inst_operand src2, union inst_operand dst)
{
    instruction *new_instruction= malloc(sizeof(instruction));
    if(new_instruction== NULL)
    {
        error(ERR_INTER);
    }
    new_instruction->type= type;
    new_instruction->src1= src1;
    new_instruction->src2= src2;
    new_instruction->dst= dst;
    return new_instruction;
}

void inst_fill(instruction *inst, enum inst_type type, union inst_operand src1, union inst_operand src2, union inst_operand dst)
{
    inst->type= type;
    inst->src1= src1;
    inst->src2= src2;
    inst->dst= dst;
}
