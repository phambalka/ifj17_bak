#ifndef STATE_H_
#define STATE_H_

typedef enum {
    S_INIT,
    S_IDENTIFIER,
    S_INTEGER,

    S_DOUBLE_DECIMAL,
    S_DOUBLE_DECIMAL_FIN,
    S_DOUBLE_EXP,
    S_DOUBLE_EXP_SIGN,
    S_DOUBLE_EXP_FIN,

    S_STRING_QUOT_BEGIN,
    S_STRING,

} ScannerState;

#endif // STATE_H_
