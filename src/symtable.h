/*

*/

#ifndef SYMTABLE_H
#define SYMTABLE_H

#include "variable.h"
//#include "function.h"

typedef enum
{
    type_var,
    type_funct
}node_type;

typedef struct btnode
{
    char *key;
    node_type data_type;
    struct btnode *left;
    struct btnode *right;
    union 
    {
        variable *var;
        //function *funct;
    }data;
} btnode;

typedef struct btree
{
    btnode *root;
}btree;


/**
 * Funkcia, ktora vyhlada a vrati polozku v binarnom strome. Ak sa polozka 
 * nenajde vrati sa NULL.
 * @param root Ukazatel na korenovy uzol v binaronom strome
 * @param key Kluc, ktory hladame
 * @return Ak sa polozka najde, vrati ukazovatel na nu, inak vrati NULL
 */
btnode *btree_find(btnode *root, char *key);

/**
 * Funkcia, ktora inicializuje binarny strom.
 * @return Ukazatel na inicializovany binarny strom
 */
btree *btree_init();

/**
 * Funkcia, ktora z retazca vytvori hash a vrati ho
 * @return Vrati hash hodnotu retazca
 */
unsigned long hash(char *key);

/**
 * Vlozi novu polozku do binarneho stromu
 * @param root Ukazovatel na korenovy uzol binarneho stromu
 * @param key Kluc, pod ktorym sa polozka ulozi
 */
void btree_insert(btnode **root, char *key);

/**
 * Funkcia, ktora vymaze binarny strom a uvolni naalokovanu pamet
 * @param Strom na vymazanie
 */
void btree_destroy(btree *tree);

/**
 * Funkcia, ktora rekurzivne vymaze vsetky polozky zo stromu a uvolni
 * naalokovanu pamet
 * @param node Polozka na vymazanie
 */
void btnode_destroy(btnode *node);

#endif
