
#ifndef STRING_H
#define STRING_H

/**
 * @brief Struktura typu string
 * 
 */
typedef struct
{
    char *str;
    int length;
    int alloc_memory;
} string;

/**
 * @brief Funkcia inicializuje novy string
 * 
 * @return string* Vrati odkaz na novo vytvoreny string
 */
string *str_init();

/**
 * @brief Funkcia premaze obsah stringu
 * 
 * @param s String, ktory chceme premazat
 */
void str_clear(string *s);

/**
 * @brief Funkcia odstrani string
 * 
 * @param s String, ktory chceme odstranit
 */
void str_free(string *s);

/**
 * @brief Funkcia prida retazec na koniec stringu
 * 
 * @param s String, do ktoreho chceme pridat retazec
 * @param s2 Retazec, ktory chceme pridat
 */
void str_append(string *s, char *s2);

/**
 * @brief Funkcia prida znak na koniec stringu
 * 
 * @param s String, do ktoreho chceme pridat znak
 * @param c Znak, ktory chceme pridat
 */
void str_append_char(string *s, char c);

/**
 * @brief Funkcia vrati dlzku retazca
 * 
 * @param s String, ktoreho dlzku chceme 
 * @return int Vrati dlzku retazca
 */
int str_length(string s);

/**
 * @brief Funkcia vrati podretazec stringu s
 * 
 * @param s String, z ktoreho chceme ziskat podretazec
 * @param i Index, na ktorom zacina podretazec, ktory chceme vratit
 * @param n dlzka podreatazca, ktory chceme vratit
 * @return string* vrati najdeny podretazec stringu
 */
string *str_substr(string s, int i, int n);

/**
 * @brief Funkcia vrati ASCII hodnotu na indexe i v stringu s
 * 
 * @param s String z ktoreho znak chceme vratit
 * @param i Index na ktorom je znak, ktory chceme vratit
 * @return int Vrati ASCII hodnotu znaku
 */
int str_asc(string s, int i);

/**
 * @brief Funkcia vrati jednoznakovy retazec s so znakom s ASCII hodnotou i
 * 
 * @param i ASCII hodnota znaku, ktory chceme vloziy
 * @return string* Vrati jednoznakovy retazec
 */
string *str_chr(int i);

/**
 * @brief Funkcia prevedie string na velke pismena
 * 
 * @param s String, ktory chceme prevadzat
 * @return char* Vrati string prevedeny na velke pismena
 */
char *str_toupeer(string s);

#endif
