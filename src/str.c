// C
#include <ctype.h>
#include <float.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#ifndef STRING_MAX
#  include <limits.h>
#  define  STRING_MAX USHRT_MAX
#endif // STRING_MAX

// Own
#include "str.h"

struct string_
{
    char*    str;
    SizeType size;
    SizeType length;
};

const SizeType str_realloc_step = 8;

static void str_init(string*);
static string* str_new_priv();

static SizeType str_compute_step_length(SizeType size)
{
    return (size / str_realloc_step + 1) * str_realloc_step;
}

static bool str_calloc(string* s, SizeType size)
{
    if (!s)
        return false;

    if (s->size >= size)
        return true;

    char* new_str = (char*) realloc(s->str, sizeof(char) * size);
    if (!new_str)
        return false;

    s->str  = new_str;
    s->size = size;
    memset(s->str + s->length, 0, s->size - s->length);

    return s;
}

static bool str_calloc_double(string* s)
{
#define GROW_RATE 1

    if (!s || (s->size << GROW_RATE) > STRING_MAX)
        return false;

    return str_calloc(s, s->size << GROW_RATE);
}

static string* str_new_priv()
{
    string* s = (string*) malloc(sizeof(string));
    if (!s)
        return NULL;
    str_init(s);
    return s;
}

void str_init(string *s)
{
    s->str    = NULL;
    s->size   = 0;
    s->length = 0;
}

string* str_new_length(SizeType length)
{
    string* s = str_new_priv();
    if (!s)
        return NULL;

    if (!str_calloc(s, str_compute_step_length(length))) {
        str_free(s);
        return NULL;
    }

    return s;
}

string* str_new()
{
    return str_new_length(0);
}

string* str_new_const_char(const char* data)
{
    SizeType length = strlen(data);
    string* s = str_new_length(length);
    if (!s)
        return NULL;

    s->length = length;
    strncpy(s->str, data, s->length);

    return s;
}

string* str_new_substr(string* s, SizeType pos, SizeType count)
{
    string* dst = str_new();

    if (!s || pos <= 0 || pos > s->length) {
        // We can't copy from this index!
        return dst;
    }

    if (count > s->length - pos || count < 0)
        count = s->length;

    str_ncpy_const(dst, s->str + pos, count);

    return dst;
}

string* str_new_chr(int i)
{
    string* s = str_new();
    if (i < 0 || i > 255)
        return s;

    str_append(s, (char) i);

    return s;
}

void str_free(string* s)
{
    if (!s)
        return;

    if (s->str) {
        free((void*) s->str);
        s->str = NULL;
    }

    free((void*) s);
}

char* str_get(string* s)
{
    return s->str;
}

char str_get_n(string* s, SizeType pos)
{
    if (!s || pos < 0 || pos > s->length)
        return 0;
    return s->str[pos];
}

int str_length(string* s)
{
    return s->length;
}

bool str_empty(string* s)
{
    if (!s)
        return true;
    return s->length == 0;
}

bool str_append(string* s, char c)
{
    if (!s)
        return false;

    if (s->length + 1 >= s->size) {
        if (!str_calloc_double(s))
            return false;
    }

    s->str[s->length] = c;
    s->str[++s->length] = '\0'; // TODO: do we need this?

    return true;
}

void str_clear(string *s)
{
    if (!s)
        return;
    if (s->str)
        free((void*) s->str);
    str_init(s);
}

bool str_cpy(string* dst, string* src)
{
    if (!dst || !src || !src->str)
        return false;

    if (!dst->str || dst->size < src->length + 1) {
        if (!str_calloc(dst, str_compute_step_length(src->length + 1)))
            return false;
    }

    strncpy(dst->str, src->str, src->length + 1);
    dst->length = src->length;

    return true;
}

// TODO: refactor
bool str_cpy_const(string* dst, const char* src)
{
    if (!dst || !src)
        return false;

    SizeType srcLen = strlen(src);
    if (dst->size < srcLen + 1) {
        if (!str_calloc(dst, str_compute_step_length(srcLen + 1)))
            return false;
    }

    strncpy(dst->str, src, srcLen + 1);
    dst->length = srcLen;

    return true;
}

bool str_ncpy_const(string* dst, const char* src, SizeType n)
{
    if (!dst || !src)
        return false;

    if (!str_calloc(dst, str_compute_step_length(n + 1)))
        return false;

    strncpy(dst->str, src, n);
    dst->length = n;

    return true;
}

bool str_cat(string* dst, string* s1, string* s2)
{
    if (!s1 || !s2 || !dst ||
        (s1->length + 1) > (STRING_MAX - s2->length))
    {
        return false;
    }

    if (!str_calloc(dst, s1->length + s2->length + 1))
        return false;

    strncpy(dst->str, s1->str, s1->length);
    strncpy(dst->str + s1->length, s2->str, s2->length);

    dst->length = s1->length + s2->length;
    dst->str[dst->length] = '\0';

    return true;
}

int str_cmp(string* s1, string* s2)
{
    return strcmp(str_get(s1), str_get(s2));
}

int str_cmp_const(string* s1, const char* s2)
{
    return strcmp(str_get(s1), s2);
}

int str_asc(string* s, SizeType pos)
{
    if (!s || pos < 0 || pos > s->length)
        return 0;
    return (int) s->str[pos];
}

bool str_to_int(string* s, int* data)
{
    if (!s || !data)
        return false;

    char* endptr = NULL;
    *data = (int) strtol(s->str, &endptr, 10);

    if (endptr)
        return false;

    return true;
}

bool str_to_double(string* s, double* data)
{
    if (!s || !data)
        return false;

    char* endptr = NULL;
    *data = (double) strtod(s->str, &endptr);

    if (endptr)
        return false;

    return true;
}
