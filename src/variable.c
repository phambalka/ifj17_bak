
#include <stdlib.h>
#include <string.h>
#include "variable.h"
#include "error.h"

variable *var_create(variable_type type, char *name)
{
    variable *var= malloc(sizeof(variable));
    if(var== NULL)
    {
        error(ERR_INTER);
    }
    var->name= malloc((strlen(name)+1)*sizeof(char));
    if(var->name== NULL)
    {
        free(var);
        error(ERR_INTER);
    }
    switch(type)
    {
        case type_string:
            var->data.str= str_init();
            break;
        case type_int:
            var->data.i= 0;
            break;
        case type_double:
            var->data.d= 0.0;
            break;
    }
    strcpy(var->name, name);
    return var;
}

void var_update(variable *var, variable_data data)
{
    var->data= data;
}