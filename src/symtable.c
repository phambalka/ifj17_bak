/*

*/

#include <stddef.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "symtable.h"

unsigned long hash(char *key)
{
    unsigned long  hash_num = 0;
    const unsigned char *p;
    for(p=(const unsigned char*)key ; *p != '\0'; p++)
    {
        hash_num = 65599 * hash_num + *p;
    }
    return hash_num;
}

btree *btree_init()
{
    btree *tree = malloc(sizeof(btree));
    if(tree == NULL)
    {
        exit(99);
    }
     tree->root = NULL;

    return tree;
}

void btree_destroy(btree *tree)
{
    if(tree->root != NULL)
    {
       btnode_destroy(tree->root);
    }
    free(tree);
}

void btnode_destroy(btnode *node)
{
    if(node->left != NULL)
    {
        btnode_destroy(node->left);
    }
    if(node->right != NULL)
    {
        
        btnode_destroy(node->right);
    }
    free(node->key);
    free(node);
}

btnode *btree_find(btnode *root, char *key)
{
    if(root == NULL || strcmp(root->key, key) == 0)
    {
        return root;
    }
    else
    {
        if(hash(root->key) < hash(key))
        {
            return btree_find(root->right, key);
        }
        else
        {
            return btree_find(root->left, key);
        }
    }
}

void btree_insert(btnode **root, char *key)
{
    if(*root == NULL)
    {
        *root = (btnode *)malloc(sizeof(btnode));
        if(root == NULL)
        {
            exit(99);
        }

        (*root)->key = (char *)malloc(sizeof(char)*(strlen(key)+1));
        if((*root)->key == NULL)
        {
            free(*root);
            exit(99);
        }
    
        strcpy((*root)->key, key);
        (*root)->data_type = 1;
        (*root)->left = NULL;
        (*root)->right = NULL;
    }
    else
    {
        if(hash((*root)->key) > hash(key))
        {
            btree_insert(&(*root)->left, key);
        }
        else
        {
           btree_insert(&(*root)->right, key);
        }
    }
}