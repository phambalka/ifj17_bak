#ifndef STR_H_
#define STR_H_

#include <stdbool.h>
#include <unistd.h>

typedef struct string_ string;
typedef ssize_t SizeType;

string* str_new();
string* str_new_const_char(const char* data);
string* str_new_length(ssize_t length);
string* str_new_substr(string* s, SizeType pos, SizeType count);
string* str_new_chr(int i);
void    str_free(string*);

char*   str_get(string* s);
char    str_get_n(string* s, SizeType pos);
int     str_length(string* s);
bool    str_empty(string* s);
bool    str_append(string* s, char c);
void    str_clear(string* s);
bool    str_cpy(string* dst, string* src);
bool    str_cpy_const(string* dst, const char* src);
bool    str_ncpy_const(string* dst, const char* src, SizeType n);
bool    str_cat(string* dst, string* s1, string* s2);
int     str_cmp(string* s1, string* s2);
int     str_cmp_const(string* s1, const char* s2);

int     str_asc(string* s, SizeType pos);

bool    str_to_int   (string* s, int*    data);
bool    str_to_double(string* s, double* data);

#endif // STR_H_
