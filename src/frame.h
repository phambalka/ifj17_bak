
#ifndef FRAME_H
#define FRAME_H

#include "variable.h"

/**
 * @brief Maximalna velkost zasobnika ramcov
 * 
 */
#define MAX_FRAMESTACK_SIZE 10

/**
 * @brief Struktura polozky ramca
 * 
 */
typedef struct framelistitem
{
    variable *var;
    struct framelistitem *next;
}FItem;

/**
 * @brief Struktura ramca
 * 
 */
typedef struct
{
    FItem *first;
}Frame;

/**
 * @brief Struktura zasobnika ramcov
 * 
 */
typedef struct
{
    Frame *stack[MAX_FRAMESTACK_SIZE];
    int top; // ukazuje na prvy volny prvok zasobniku
}FrameStack;

/**
 * @brief Funkcia na vytvorenie ramca
 * 
 * @return Frame* Vrati odkaz na novo vytvoreny ramec
 */
Frame *frame_create();

/**
 * @brief Funkcia vlozi ramec na vrchol zasobnika
 * 
 * @param f Ramec, ktory chceme vlozit
 * @param fs Zasobnik, do ktoreho chceme vlozit ramec
 */
void frame_push(Frame *f, FrameStack *fs);

/**
 * @brief Funkcia odstrani ramec z vrcholu zasobnika
 * 
 * @param f Docasny ramec, do ktoreho si ulozime ramec z vrcholu zasobnika
 * @param fs Zasobnik, z ktoreho chceme ramec odstranit
 */
void frame_pop(Frame *f, FrameStack *fs);

/**
 * @brief Funkcia definuje novu premennu v ramci f
 * 
 * @param f Ramec, do ktoreho chceme vlozit novu premmenu
 * @param var Premenna, ktoru chceme vlozit do ramca
 */
void frame_defvar(Frame *f, variable *var);

/**
 * @brief Funkcia vrati ramec z vrcholu zasobnika
 * 
 * @param fs Zasobnik, ktoreho vrchol chceme vratit
 * @return Frame* Vrati odkaz na ramec z vrcholu zasobnika
 */
Frame *framestack_top(FrameStack *fs);

/**
 * @brief Funkcia inicializuje novy zasobnik ramcov
 * 
 * @return FrameStack* Vrati odkaz na novo vytvoreny zasobnik
 */
FrameStack *framestack_init();

#endif