#ifndef LIST_H
#define LIST_H

#include <stdbool.h>

#include "instruction.h"

/**
 * @brief Struktura polozky zoznamu
 * 
 */
typedef struct listitem
{
    instruction *inst;
    struct listitem *lptr;
    struct listitem *rptr;
}LItem;

/**
 * @brief Struktura zoznamu instrukcii
 * 
 */
typedef struct
{
    LItem *first;
    LItem *last;
    LItem *act;
}List;

/**
 * @brief Funkcia inicializuje zoznam instrukcii
 * 
 * @param list Zoznam, ktory chceme inicializovat
 */
void list_init(List *list);

/**
 * @brief Funkcia posunie aktivitu o jednu polozku doprava
 * 
 * @param list Zoznam, v ktorom chceme aktivitu posunut
 */
void list_succ(List *list);

/**
 * @brief Funkcia vlozi instrukciu na koniec zoznamu
 * 
 * @param list Zoznam, do ktoreho chceme vlozit instrukciu
 * @param inst Instrukcia, ktoru chceme vlozit
 */
void list_insert(List *list, instruction *inst);

/**
 * @brief 
 * 
 * @param list 
 */
void list_dispose(List *list);

/**
 * @brief Funkcia skontroluje, ci je zoznam prazdny
 * 
 * @param list Zoznam, ktory chceme skontrolovat
 * @return true Vrati true ak je zoznam prazdny
 * @return false Vrati false ak zoznam nie je prazdny
 */
bool is_list_empty(List *list);


#endif