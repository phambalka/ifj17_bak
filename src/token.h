#ifndef TOKEN_H_
#define TOKEN_H_

// Own
#include "str.h"

typedef enum {
    T_UNKNOWN,
    T_LEX_ERROR,
    T_IDENTIFIER,
    T_INTEGER,
    T_DOUBLE,
    T_STRING,
} TokenType;

typedef struct {
    TokenType type;
    union {
        string* sval;
        int     ival;
        double  dval;
    } data;
} Token;

void token_init(Token* t);

#endif // TOKEN_H_
