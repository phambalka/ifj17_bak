#ifndef SCANNER_H_
#define SCANNER_H_

// Own
#include "token.h"

int    scanner_open(const char* filename);
int    scanner_close();
Token* scanner_get_token();

#endif // SCANNER_H_
