
#ifndef VARIABLE_H
#define VARIABLE_H

#include <stdbool.h>
#include "string.h"

/**
 * @brief Zoznam typov premennej
 * 
 */
typedef enum 
{
    type_string,
    type_int,
    type_double
} variable_type;

/**
 * @brief Obsah premennej
 * 
 */
typedef union
{
    string *str;
    int i;
    double d;
}variable_data;

/**
 * @brief Struktura premennej
 * 
 */
typedef struct variable
{
    char *name;
    variable_type v_type;
    bool constant;
    variable_data data;
}variable;

/**
 * @brief Funkcia vytvori novu premennu
 * 
 * @param type Typ vytvaranej premmenj
 * @param name Meno vytvaranej premennej
 * @return variable* Vrati odkaz na novo vytvorenu premennu
 */
variable *var_create(variable_type type, char *name);

/**
 * @brief Funkcia aktualizuje obsah premennej
 * 
 * @param var Premenna, ktoru chceme aktualizovat
 * @param d Novy obsah premennej
 */
void var_update(variable *var, variable_data d);

#endif