
#ifndef FUNCTION_H
#define FUNCTION_H

#include <stdbool.h>
#include "variable.h"

/**
 * @brief Struktura funkcie
 * 
 */
typedef struct 
{
    char *name;
    variable_type return_type; 
    unsigned int params_count;
    bool defined;
    //Zoznam istrukcii
    struct btree *local_tree;
    variable *return_var;
}function;

#endif