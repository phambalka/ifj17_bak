#include <string.h>
#include <ctype.h>
#include <stdlib.h>
#include "string.h"
#include "error.h"
string *str_init()
{
    string *s = malloc(sizeof(string));
    if(s == NULL)
    {
        error(ERR_INTER);
    }
    s->str = malloc(sizeof(char));
    if(s->str == NULL){
        free(s);
        error(ERR_INTER);
    }
    s->str[0] = '\0';
    s->length = 0;
    s->alloc_memory = 1;
    return s;
}

void str_clear( string *s)
{
    if(s != NULL)
    {
        free(s->str);
        s->str = malloc(sizeof(char));
        if(s->str == NULL){
            free(s);
            error(ERR_INTER);
        }
        s->str[0] = '\0';
        s->length = 0;
        s->alloc_memory = 1;
    }
}

void str_free( string *s)
{
    if(s != NULL)
    {
        free(s->str);
        free(s);
    }
}

void str_append( string *s, char *s2)
{
    s->str = (char *)realloc(s->str,(s->length+strlen(s2)+1)*sizeof(char));
    if(s == NULL)
    {
        error(ERR_INTER);
    }
    strcat(s->str, s2);
    s->length+=strlen(s2);
    s->str[s->length] = '\0';
}

void str_append_char( string *s, char c)
{
    s->str = (char *)realloc(s->str,(s->length+2)*sizeof(char));
    if(s == NULL)
    {
        error(ERR_INTER);
    }
    s->str[s->length] = c;
    s->length++;
    s->str[s->length] = '\0';
}

int str_length( string s)
{
    return s.length;
}

int str_asc( string s, int i)
{
    if(i >= 0 && i < s.length)
    {
        return (int)s.str[i];
    }
    return 0;
}

string *str_chr(int i){
    if(i >= 0 && i <= 255)
    {
        string *s = str_init();
        str_append_char(s, i);
        return s;
    }
    return NULL;
}

string *str_substr(string s, int i, int n){
    if(i >= 0 && i <= s.length)
    {
        if(n<0 || n> s.length-i)
        {
            char s2[s.length-i+1];
            for(int j=i;j<s.length;j++)
            {
                s2[j-i]= s.str[j];
            }
            s2[s.length-i]='\0';

            string *s3;
            s3 = str_init();
            str_append(s3,s2);
            s3->length = s.length-i;
            s3->alloc_memory = s.length-i+1;
            return s3;
        }
        else
        {
            char s2[n+1];
            int j = 0;
            char c;
            while((c = s.str[i++])!='\0')
            {
                s2[j++] = c;
            }
            s2[n]='\0';

            string *s3;
            s3 = str_init();
            str_append(s3,s2);
            s3->length = s.length-i;
            s3->alloc_memory = s.length-i+1;
            return s3;
        }
    }
    else
    {
        char *s2= "\0";
        string *s3;
        s3 = str_init();
        str_append(s3,s2);
        s3->length = 0;
        s3->alloc_memory = 1;
        return s3;
    }

}

char *str_toupeer(string s)
{
    char *str= malloc(sizeof(char)*(str_length(s)+1));
    if(str== NULL){
        error(ERR_INTER);
    }
    int i=0;
    while(s.str[i]!='\0')
    {
        str[i]= toupper(s.str[i]);
        i++;
    }
    str[i]= '\0';
    return str;
}