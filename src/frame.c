
#include <stdlib.h>
#include "frame.h"
#include "error.h"

Frame *frame_create()
{
    Frame *f= malloc(sizeof(Frame));
    if(f== NULL)
    {
        error(ERR_INTER);
    }
    f->first= NULL;
    return f;
}

void frame_defvar(Frame *f, variable *var)
{
    if(f->first== NULL)
    {
        FItem *item= malloc(sizeof(FItem));
        if(item== NULL)
        {
            error(ERR_INTER);
        }
        item->var= var;
        item->next= NULL;
        f->first= item;
    }
    else
    {
        FItem *item= f->first;
        while(item->next!= NULL)
        {
            item= item->next;
        }
        FItem *newitem= malloc(sizeof(FItem));
        if(newitem== NULL)
        {
            error(ERR_INTER);
        }
        newitem->var= var;
        newitem->next= NULL;
        item->next= newitem;
    }
}

void frame_push(Frame *f, FrameStack *fs)
{
    if(fs->top< MAX_FRAMESTACK_SIZE)
    {
        fs->stack[fs->top]= f;
        fs->top++;
    }
    else
    {
        error(ERR_INTER);
    }
}

void frame_pop(Frame *f, FrameStack *fs)
{
    f= fs->stack[fs->top];
    fs->stack[fs->top]= NULL;
    fs->top--;
}

Frame *framestack_top(FrameStack *fs)
{
    if(fs->top> 0){
        return fs->stack[fs->top-1];
    }
    else{
        return NULL;
    }
}

FrameStack *framestack_init()
{
    FrameStack *fs= malloc(sizeof(FrameStack));
    if(fs== NULL)
    {
        error(ERR_INTER);
    }
    for(int i=0; i<MAX_FRAMESTACK_SIZE; i++)
    {
        fs->stack[i]= NULL;
    }
    fs->top= 0;
    return fs;
}