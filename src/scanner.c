// C
#include <ctype.h>
#include <stdio.h>
#include <stdbool.h>
#include <string.h>

// Own
#include "err.h"
#include "scanner.h"
#include "state.h"
#include "token.h"

static FILE* fin = NULL;
static Token token;

static int row = 0;

static int get_char()
{
    int c = getc(fin);

    if (c == '\n')
        ++row;

    return c;
}

static void unget_char(int c)
{
    ungetc(c, fin);

    if (c == '\n')
        --row;
}

static void token_set_lex_error()
{
    switch (token.type) {
    case T_IDENTIFIER:
        // fall through
    case T_INTEGER:
        // fall through
    case T_DOUBLE:
        str_free(token.data.sval);
        break;
    default:
        break;
    };

    token.type = T_LEX_ERROR;
}

int scanner_open(const char* filename)
{
    fin = fopen(filename, "r");
    return fin != NULL ? ERR_None : ERR_InputFile;
}

int scanner_close()
{
    if (!fin || fclose(fin) != 0)
        return ERR_InputFile;
    fin = NULL;
    return ERR_None;
}

bool scanner_has_opened_source()
{
    return fin != NULL ;
}

Token* scanner_get_token()
{
    ScannerState state = S_INIT;
    int c;

    if (!scanner_has_opened_source())
        return NULL;

    token_init(&token);

    for (;;) {
        c = get_char();

        switch (state) {
        case S_INIT:
            if (isalpha(c) || c == '_') {
                state = S_IDENTIFIER;
                token.type = T_IDENTIFIER;
                token.data.sval = str_new_chr(c);
                break;
            } else if (isdigit(c)) {
                state = S_INTEGER;
                token.type = T_INTEGER;
                token.data.sval = str_new_chr(c);
                break;
            } else if (c == '!') {
                state = S_STRING_QUOT_BEGIN;
                token.type = T_STRING;
                break;
            }

            // No known lexeme recognized
            return NULL;

        case S_IDENTIFIER:
            if (!isalnum(c) && c != '_') {
                unget_char(c);
                return &token;
            }
            str_append(token.data.sval, c);
            break;

        case S_INTEGER:
            if (c == '.') {
                state = S_DOUBLE_DECIMAL;
                token.type = T_DOUBLE;
            } else if (c == 'e' || c == 'E') {
                state = S_DOUBLE_EXP;
                token.type = T_DOUBLE;
            } else if (!isdigit(c)) {
                unget_char(c);
                int ival;
                str_to_int(token.data.sval, &ival);
                str_free(token.data.sval);
                token.data.ival = ival;
                return &token;
            }
            str_append(token.data.sval, c);
            break;

        case S_DOUBLE_DECIMAL:
            if (!isdigit(c)) {
                token_set_lex_error();
                return &token;
            }
            state = S_DOUBLE_DECIMAL_FIN;
            str_append(token.data.sval, c);
            break;

        case S_DOUBLE_DECIMAL_FIN:
            if (c == 'e' || c == 'E') {
                state = S_DOUBLE_EXP;
            } else if (!isdigit(c)) {
                unget_char(c);
                double dval;
                str_to_double(token.data.sval, &dval);
                str_free(token.data.sval);
                token.data.dval = dval;
                return &token;
            }
            str_append(token.data.sval, c);
            break;

        case S_DOUBLE_EXP:
            if (c == '+' || c == '-') {
                state = S_DOUBLE_EXP_SIGN;
            } else if (isdigit(c)) {
                state = S_DOUBLE_EXP_FIN;
            } else {
                token_set_lex_error();
                return &token;
            }
            str_append(token.data.sval, c);
            break;

        case S_DOUBLE_EXP_SIGN:
            if (!isdigit(c)) {
                token_set_lex_error();
                return &token;
            }
            state = S_DOUBLE_EXP_FIN;
            str_append(token.data.sval, c);
            break;

        case S_DOUBLE_EXP_FIN:
            if (!isdigit(c)) {
                unget_char(c);
                double dval;
                str_to_double(token.data.sval, &dval);
                str_free(token.data.sval);
                token.data.dval = dval;
                return &token;
            }

            break;

        case S_STRING_QUOT_BEGIN:
            if (c != '"') {
                unget_char(c);
                return NULL;
            }
            state = S_STRING;
            token.data.sval = str_new();
            break;

        case S_STRING:
            if (c != '"')
                str_append(token.data.sval, c);
            else
                return &token;
        }
    }

    return NULL;
}
