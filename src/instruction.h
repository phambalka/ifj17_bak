
#ifndef INSTRUCTION_H
#define INSTRUCTION_H


/**
 * @brief Zoznam instrukcii
 * 
 */
enum inst_type
{
    /* aritmeticke */
    INST_ADD,
    INST_SUB,
    INST_MUL,
    INST_DIV,
    INST_ADDS,
    INST_SUBS,
    INST_MULS,
    INST_DIVS,

    /* relacne */
    INST_LT,
    INST_GT,
    INST_EQ,
    INST_LTS,
    INST_GTS,
    INST_EQS,

    /* booleovske */
    INST_AND,
    INST_OR,
    INST_NOT,
    INST_ANDS,
    INST_ORS,
    INST_NOTS,

    /*konverzne */
    INST_INT2FLOAT,
    INST_FLOAT2INT,
    INST_FLOAT2R2EINT,
    INST_FLOAT2R2OINT,
    INST_INT2CHAR,
    INST_STRI2INT,
    INST_INT2FLOATS,
    INST_FLOAT2INTS,
    INST_FLOAT2R2EINTS,
    INST_FLOAT2R2OINTS,
    INST_INT2CHARS,
    INST_STRI2INTS,

    /* praca so zasobnikom */
    INST_PUSHS,
    INST_POPS,
    INST_CLEARS,

    /* praca s ramcami, volania funkcii */
    INST_MOVE,
    INST_CREATEFRAME,
    INST_PUSHFRAME,
    INST_POPFRAME,
    INST_DEFVAR,
    INST_CALL,
    INST_RETURN,

    /* vstupno-vystupne */
    INST_READ,
    INST_WRITE,

    /* praca s retazcami */
    INST_CONCAT,
    INST_STRLEN,
    INST_GETCHAR,
    INST_SETCHAR,

    /* praca s typmi */
    INST_TYPE,

    /* riadenie toku programu */
    INST_LABEL,
    INST_JUMP,
    INST_JUMPIFEQ,
    INST_JUMPIFNEQ,
    INST_JUMPIFEQS,
    INST_JUMPIFNEQS,

    /* ladiace */
    INST_BREAK,
    INST_DPRINT
};

/**
 * @brief Operand instrukcie
 * 
 */
union inst_operand
{
    struct variable *var;
    struct instruction *inst;
};

/**
 * @brief Struktura instrukcie
 * 
 */
typedef struct
{
    enum inst_type type;
    union inst_operand src1;
    union inst_operand src2;
    union inst_operand dst;
}instruction;

/**
 * @brief Funkcia vytvori novu instrukciu
 * 
 * @param inst_type Instrukcia
 * @param inst_operand src1 prvy operand instrukcie
 * @param inst_operand src2 druhy operand instrukcie
 * @param inst_operand dst cielovy operand
 * @return instruction* Vrati odkaz na novo vytvorenu instrukciu
 */
instruction *inst_create(enum inst_type type, union inst_operand src1, union inst_operand src2, union inst_operand dst);

/**
 * @brief 
 * 
 * @param inst Instrukcia, ktoru chceme naplnit
 * @param inst_type Instrukcia
 * @param inst_operand src1 prvy operand instrukcie
 * @param inst_operand src2 druhy operand instrukcie
 * @param inst_operand dst cielovy operand
 */
void inst_fill(instruction *inst, enum inst_type type, union inst_operand src1, union inst_operand src2, union inst_operand dst);

#endif
