#include "list.h"
#include "error.h"

#include <stdlib.h>

void init_list(List *list)
{
    list->act = list->last = list->first = NULL;
}


void list_succ(List *list)
{
    if(!is_list_empty)
    {
        if(list->act->rptr != NULL)
        {
            list->act = list->act->rptr;
        }
    }  
}

void list_insert(List *list, instruction *inst)
{
    LItem *new_item = malloc(sizeof(LItem));
    if(new_item == NULL)
    {
        error(ERR_INTER);
    }

    new_item->inst = inst;
    if(!is_list_empty(list))
    {
        list->first = list->last = new_item;
    }
    else
    {
        list->last->rptr = new_item;
        new_item->lptr = list->last;
        list->last = new_item;
        new_item->rptr = NULL;
        
    }
}

void list_dispose(List *list)
{
    LItem *item = list->first;
    
    if(item != NULL)
    {
        while(item->rptr != NULL)
        {
            item = item ->rptr;
            free(item->lptr);
        }
        free(item);
        list->first = NULL;
        list->last = NULL;
        list->act = NULL;    
    }
}

bool is_list_empty(List *list)
{
    return list->first == NULL;
}