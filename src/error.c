#include "error.h"

#include <stdio.h>
#include <stdlib.h>

void error(int err_code)
{
    switch(err_code)
    {
        case ERR_LEX:
            fprintf(stderr, "Error 1: Nastala chyba v lexikalnej analyze.\n");
        break;

        case ERR_SYNT:
            fprintf(stderr, "Error 2: Nastala chyba v syntaktickej analyze. Chybna syntax programu.\n");
        break;

        case ERR_SEM_DEF:
            fprintf(stderr, "Error 3: Nastala chyba v sematickej analyze. Nedefinvana premenna/funkcia, alebo pokus o redefinovanie premennej/funkcie.\n");
        break;

        case ERR_SEM_TYP:
            fprintf(stderr, "Error 4: Nastala chyba v sematickej analyze. Chybna typova kompatibilita.\n");
        break;

        case ERR_SEM_OTHER:
            fprintf(stderr, "Error 6: Nastala chyba v sematickej analyze.\n");
        break;

        case ERR_INTER:
            fprintf(stderr, "Error 99: Nastala interna chyba prekladaca.\n");
        break;
    }

    //TODO: Uvolnenie pameti

    exit(err_code);
}