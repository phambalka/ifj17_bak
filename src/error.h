#ifndef ERROR_H
#define ERROR_H

enum error_code
{
    ERR_LEX = 1,
    ERR_SYNT,
    ERR_SEM_DEF,
    ERR_SEM_TYP,
    ERR_SEM_OTHER = 6,
    ERR_INTER = 99
};

void error(int err_code);

#endif